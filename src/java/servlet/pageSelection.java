package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/pageSelection"})
public class pageSelection extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String stp =  request.getParameter("Synchronizer_Token_Pattern");
        String dscp = request.getParameter("Double_Submit_Cookies_Pattern");
        
        HttpSession session = request.getSession();        
        if(stp != null){
            System.out.println("stp");
            session.setAttribute("request_demo", stp);
        }  
        else if (dscp != null){
            System.out.println("dscp");
            session.setAttribute("request_demo", dscp);
        }
        response.sendRedirect("login.jsp");        
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
